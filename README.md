## Name

Task Managment System.
 

## Installation
##### need php version >= 7.4 
##### 1- Clone the reposetory.
##### 2- cd task-managment-system-backend.
##### 3- configure .env file :
######     * database connection.
######     * mail settings.
######     * add this line "QUEUE_DRIVER=database" to .env file.
##### 4- run "composer install".
##### 5- run "php artisan key:generate".
##### 6- run "php artisan migrate --seed".
##### 7- run "php artisan passport:client --personal".
##### 8- run "php artisan serv".
##### 9- run "php artisan php artisan queue:listen".
##### 10- run "php artisan php php artisan schedule:run".

