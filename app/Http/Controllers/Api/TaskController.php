<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\TaskRequest;
use App\Models\Task;
use App\Models\User;
use App\Repositories\TaskRepository;
use Illuminate\Http\Request;

class TaskController extends ApiController
{

    private $taskRepository;
    public function __construct(TaskRepository $taskRepository)
    {
        $this->taskRepository = $taskRepository;
    }

    public function index(Request $request)
    {
        $limit = $request->get('limit') ? : 4 ;
        if($limit > 30 ) $limit =30 ;


        $tasks = $this->taskRepository
            ->getTasks($request);

        $tasks = $tasks->orderBy('id', 'desc')
                ->paginate($limit);
    


        return $this->respondSuccess($tasks->all(), $this->createApiPaginator($tasks));
    }


    public function show($id)
    {
        $task = Task::query()
        ->with(['subs','categories'])
        ->where('id', $id)
        ->first();

        if (!$task)
            return $this->respondError('item not found');

        return $this->respondSuccess($task);
    }

    public function store(TaskRequest $request)
    {
        $user = $this->getUser($request);

        if (!$user)
            return $this->respondError('user not found');

        $request['user_id'] = $user->id;

        $task = $this->taskRepository->add($request);
        return $this->respondSuccess();

    }

    public function getTasks(Request $request)
    {
        $tasks = $this->taskRepository
            ->getTasksWithOut($request);

        $tasks = $tasks->orderBy('id', 'desc')
                ->get();

        return $this->respondSuccess($tasks->all());
    }
}
