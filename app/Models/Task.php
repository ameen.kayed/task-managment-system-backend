<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Task extends Model
{
    use HasFactory;

    const FLAG = 0;

     /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'description',
        'dead_line',
        'parent_task',
        'user_id',
        'flag',
    ];

    public function user() : BelongsTo
    {
        return $this->belongsTo(User::class,'user_id','id');
    }

    public function categories() : BelongsToMany
    {
        return $this->belongsToMany(Category::class,'task_categories');
    }

    public function getCreatedAtAttribute()
    {
       return date('Y-m-d', strtotime($this->attributes['created_at']));
    }

    public function subs() : HasMany
    {
        return $this->hasMany(Task::class,'parent_task','id');
    }

    public function scopeParent(Builder $query): Builder
    {
        return $query->whereNull('parent_task');
    }

    public function scopeFlaged(Builder $query): Builder
    {
        return $query->where('flag',Task::FLAG);
    }

    public function scopeEnded(Builder $query): Builder
    {
        return $query->where('dead_line','<=',Carbon::now());
    }

    public function scopeSub(Builder $query): Builder
    {
        return $query->whereNotNull('parent_task');
    }
}
