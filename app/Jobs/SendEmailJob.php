<?php

namespace App\Jobs;

use App\Mail\SendMails;
use App\Models\User;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tasks;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($tasks)
    {
        $this->tasks = $tasks;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $tasks = $this->tasks;
        $emails = User::select('email')->get()->toArray();
        foreach ($tasks as $task){
            $task->flag = 1;
            $task->save();
            $email = new SendMails($task);
            try{
                if(count($emails) > 0)
                    Mail::to($emails)->send($email);
            }
            catch(Exception $e){
                echo $e;
            }
        }
    }
}
