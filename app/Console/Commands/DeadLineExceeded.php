<?php

namespace App\Console\Commands;

use App\Jobs\SendEmailJob;
use App\Models\Category;
use App\Models\Task;
use Carbon\Carbon;
use Illuminate\Console\Command;

class DeadLineExceeded extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dayly:tasks';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'make tasks ended and send mails';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $tasks = Task::parent()->ended()->flaged()->get();
        dispatch(new SendEmailJob($tasks));
    }
}
