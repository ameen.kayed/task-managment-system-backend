<?php

namespace App\Repositories;

use App\Http\Traits\ShiftTrait;
use App\Models\Admin;
use App\Models\Category;
use App\Models\Task;
use Carbon\Carbon;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class TaskRepository {

    public function add(Request $request)
    {
        $task = new Task();
        $task->description = $request->description;
        $task->dead_line = $request->dead_line;
        $task->user_id = $request->user_id;

        if($request->has('parent_task'))
            $task->parent_task = $request->parent_task;

        $task->save();

        $task->categories()->sync($request->category_id);

    }

    public function getTasks(Request $request): Builder
    {
        $tasks = Task::query()->parent()->with(['subs','categories','user']);

        if($request->has('search') && $request->search != '')
            $tasks->whereRaw("description  LIKE '%" .$request->search."%'" );
        return $tasks;
    }

    public function getTasksWithOut(Request $request): Builder
    {
        $tasks = Task::query()->parent();

        return $tasks;
    }

}
