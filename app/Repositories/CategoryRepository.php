<?php

namespace App\Repositories;

use App\Models\Category;
use App\Models\CategoryBrand;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Spatie\LaravelImageOptimizer\Facades\ImageOptimizer;
use Illuminate\Database\Eloquent\Builder;



class CategoryRepository {

    
    public function getCategories(Request $request, $categoryId = null, $withSubs = false): Builder
    {
        $categories = Category::query();
        return $categories;
    }
    
}
