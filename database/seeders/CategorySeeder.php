<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cat = Category::count();
        if($cat == 0){
           Category::create([
            'name' => 'To Do',
            'color' => 'gray',
           ]);

           Category::create([
            'name' => 'In Progress',
            'color' => 'red',
           ]);

           Category::create([
            'name' => 'Testing',
            'color' => 'orange',
           ]);

           Category::create([
            'name' => 'Ended',
            'color' => 'blue',
           ]);
        }
    }
}
